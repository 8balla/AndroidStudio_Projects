package uk.co.dnbhydro.www.dnbhydro;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {
    private static final String TEL_PREFIX = "tel:";
    private WebView mywebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mywebView = (WebView) findViewById(R.id.WebView);
        WebSettings webSettings = mywebView.getSettings();
        mywebView.setWebViewClient(new WebViewClient());
        mywebView.setWebViewClient(new CustomWebViewClient());
        webSettings.setJavaScriptEnabled(true);
        mywebView.loadUrl("https://dnbhydro.co.uk/");
    }
        //code for call button

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }


        private class CustomWebViewClient extends WebViewClient {

            @Override
            public boolean shouldOverrideUrlLoading(WebView wv, String url) {
                if (url.startsWith(TEL_PREFIX)) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        }
    //Code For Back Button
    @Override
    public void onBackPressed() {
        if(mywebView.canGoBack())
        {
            mywebView.goBack();
        }

        else
        {
            super.onBackPressed();
        }
    }
}